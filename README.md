# Ansible Docker

ansible - https://www.ansible.com

Latest ansbile release in pip embedded into this docker.

Used to perform ansible playbooks into gitlab ci runner.

## How to use

### Pull image

```
docker pull registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible:latest
```

or for alpine version:
```
docker pull registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible:latest-alpine
```

## Run container

Per default, the docker will give you the version number of ansible.

If you want to launch a playbook, you'll have to run a specific command: 

```
docker run --rm -v <path for playbook folder>:/playbooks registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible:latest-alpine ansible-playbook yourplaybook.yaml
```

## In gitlab ci runner

Here's an example of a working configuration (that have a `deploy` stage):

```
ansible:
  stage: deploy
  image: registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible:latest-alpine
  script:
    - ansible-playbook -i inventory myplaybook.yaml
```
